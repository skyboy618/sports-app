package com.ionut.SportsApp.controller;

import com.ionut.SportsApp.model.City;
import com.ionut.SportsApp.service.city.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/city")
public class CityController {

    private final CityService cityService;

    @Autowired
    public CityController(CityService cityService) {
        this.cityService = cityService;
    }

    @PostMapping("/add")
    public ResponseEntity<City> add(@RequestBody City city){
        return ResponseEntity.ok(cityService.add(city));
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<Optional<City>> getById(@PathVariable Integer id){
        return ResponseEntity.ok(cityService.getById(id));
    }

    @GetMapping("/get")
    public List<City> getAll(){
        return cityService.getAll();
    }

    @DeleteMapping("/delete/{id}")
    public void deleteById(@PathVariable Integer id){
        cityService.deleteById(id);
    }

    }

