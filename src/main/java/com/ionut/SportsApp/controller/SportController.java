package com.ionut.SportsApp.controller;

import com.ionut.SportsApp.model.Sport;
import com.ionut.SportsApp.service.sport.SportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/sport")
public class SportController {


    private final SportService sportService;

    @Autowired
    public SportController(SportService sportService) {
        this.sportService = sportService;
    }

    @PostMapping("/add")
    public ResponseEntity<Sport> add(@RequestBody Sport sport) {
        return ResponseEntity.ok(sportService.add(sport));
    }


    @DeleteMapping("/delete/{id}")
    public void deleteSport(@PathVariable Integer id) {
        sportService.deleteById(id);
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<Optional<Sport>> getById (@PathVariable Integer id){
        return ResponseEntity.ok(sportService.getById(id));
    }

    @GetMapping("get")
    public List<Sport> getAll(){
        return sportService.getAll();
    }

}
