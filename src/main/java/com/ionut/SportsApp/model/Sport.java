package com.ionut.SportsApp.model;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Objects;

@Entity
public class Sport {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private String name;

    @Column
    private Double costPerDay;

    @Column
    private Calendar availableTime;

    @ManyToOne
    @JoinColumn(name = "city")
    private City city;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getCostPerDay() {
        return costPerDay;
    }

    public void setCostPerDay(Double costPerDay) {
        this.costPerDay = costPerDay;
    }

    public Calendar getAvailableTime() {
        return availableTime;
    }

    public void setAvailableTime(Calendar availableTime) {
        this.availableTime = availableTime;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sport sport = (Sport) o;
        return Objects.equals(id, sport.id) && Objects.equals(name, sport.name) && Objects.equals(costPerDay, sport.costPerDay) && Objects.equals(availableTime, sport.availableTime) && Objects.equals(city, sport.city);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, costPerDay, availableTime, city);
    }
}
