package com.ionut.SportsApp.service.sport;

import com.ionut.SportsApp.model.Sport;
import com.ionut.SportsApp.repository.SportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SportServiceImpl implements SportService {

    private final SportRepository sportRepository;

    @Autowired
    public SportServiceImpl(SportRepository sportRepository) {
        this.sportRepository = sportRepository;
    }


    @Override
    public Sport add(Sport sport) {
        return sportRepository.save(sport);
    }

    @Override
    public Optional<Sport> getById(Integer id) {
        return sportRepository.findById(id);
    }

    @Override
    public List<Sport> getAll() {
        return sportRepository.findAll();
    }

    @Override
    public void deleteById(Integer id) {
        sportRepository.deleteById(id);
    }
}
