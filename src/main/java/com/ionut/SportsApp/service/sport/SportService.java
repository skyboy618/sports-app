package com.ionut.SportsApp.service.sport;

import com.ionut.SportsApp.model.Sport;

import java.util.List;
import java.util.Optional;

public interface SportService {

        public Sport add(Sport sport);

        public Optional<Sport> getById(Integer id);

        public List<Sport> getAll();

        public void deleteById(Integer id);


}
