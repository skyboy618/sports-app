package com.ionut.SportsApp.service.city;

import com.ionut.SportsApp.model.City;
import com.ionut.SportsApp.model.Sport;

import java.util.List;
import java.util.Optional;

public interface CityService {


    public City add(City city);

    public Optional<City> getById(Integer id);

    public List<City> getAll();

    public void deleteById(Integer id);
}
