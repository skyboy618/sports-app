package com.ionut.SportsApp.service.city;

import com.ionut.SportsApp.model.City;
import com.ionut.SportsApp.model.Sport;
import com.ionut.SportsApp.repository.CityRepository;
import com.ionut.SportsApp.service.city.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CityServiceImpl implements CityService {

    private final CityRepository cityRepository;

    @Autowired
    public CityServiceImpl(CityRepository cityRepository) {
        this.cityRepository = cityRepository;
    }


    @Override
    public City add(City city) {
        return cityRepository.save(city);
    }

    @Override
    public Optional<City> getById(Integer id) {
       return cityRepository.findById(id);
    }

    @Override
    public List<City> getAll() {
        return cityRepository.findAll();
    }

    @Override
    public void deleteById(Integer id) {
        cityRepository.deleteById(id);
    }
}
